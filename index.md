---
type: docs
title: GPIO
date: 2024-01-25T23:04:56+08:00
featured: false
draft: false
comment: true
toc: true
pinned: false
carousel: false
series: 
  - 
categories: 
  - 
tags: 
  - 
authors: 
  - 
images: 
copyright: false
# navWeight: 1000
---

# GPIO

內建 LED 閃爍測試

依 UM1724 6.4 LEDs 文件可知，User LD2 同 Arduino 位在 D13 也就是 PA5。
- the I/O is HIGH value, the LED is on
- the I/O is LOW, the LED is off

<!--more-->

控制腳位狀態的暫存器

- RCC_AHBENR：外設時脈開關
- GPIOx_MODER：腳位模式設定
- GPIOx_OTYPER：推挽 (預設)、開漏
- GPIOx_OSPEEDER：Output Speed Register (default: Low speed)
- GPIOx_PUPDR：上、下拉電阻 (預設無)

控制腳位輸出的暫存器

- GPIOX_ODR：設定輸出高低
- GPIOx_BSRR：設定/重置輸出高低
- GPIOx_BRR：重置輸出高低

